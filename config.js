var mongoose = require('mongoose'); //library file
var multer  = require('multer'); //file upload middleware
var dbConnected = false;

if (!dbConnected) {
    //connect to the db
    mongoose.connect('mongodb://localhost/nodetest1');
    dbConnected = true;    
}

exports.db = mongoose;

exports.uploadHelper = multer({ dest: './uploads/'});