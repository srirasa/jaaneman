var config = require('../config.js');

var db = config.db;

var userSchema = db.Schema({
  username: String,
  email: String,
  createdAt: { type: Date, default: Date.now }
});

var User = db.model('User', userSchema);

module.exports =  User;

