var express = require('express');
var config  = require('../config.js');
var router = express.Router();

router.get('/new', function(req, res) {
    res.render('newimage', { title: 'Add New Image' });
});

router.use(config.uploadHelper);


router.post('/addfile', function(req, res) {
    
    console.log('JSON FILE OBJECT', req.files);
    console.log('JSON BODY OBJECT', req.body);
    res.send(req.files.addFileName.originalname
        + ' Uploaded Successfully as ' + req.files.addFileName.path);
});

module.exports = router;
