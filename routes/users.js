var express = require('express');
var router = express.Router();

var User = require('../models/user')

router.get('/', function (req, res, next) {
  User.find(function (err, docs) {
    if (err) return next(err);
    res.render('userlist', {
            "userlist" : docs
     });
  });
});

router.get('/newuser', function(req, res) {
    res.render('newuser', { title: 'Add New User' });
});

router.post('/adduser', function(req, res) {

    // Set our internal DB variable

    // Get our form values. These rely on the "name" attributes
    var userName = req.body.username;
    var userEmail = req.body.useremail;
    newUser = new User({
        "username" : userName,
        "email" : userEmail
    });
    
    newUser.save(function (err, newUser) {
        if (err) {
            console.log('error is ', err);
            // If it failed, return error
            res.send("There was a problem adding the information to the database.");
        }
        else {
            console.log('user is added successfull ', newUser);
            // If it worked, set the header so the address bar doesn't still say /adduser
            res.location("/users");
            // And forward to success page
            res.redirect("/users");
        }
    });
});


/*
router.add = function (req, res, next) {
  model.create({ name: 'inserting ' + Date.now() }, function (err, doc) {
    if (err) return next(err);
    res.send(doc);
  })
}
*/

module.exports = router;
